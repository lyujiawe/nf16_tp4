#include "tp4_cousu.h"

T_Noeud_C * noeud_pere(T_Noeud_C *n,T_Arbre_C *abr){
    T_Noeud_C * p = *abr;
    if (n == p) {
        p = NULL;
        return p;
    }
    while(p->FD!=n && p->FG!=n){
        if(p->cle > n->cle)
            p = p->FG;
        else
            p = p->FD;
    }
    return p;
}



T_Noeud_C *cousu_creer_noeud(int valeur){
    T_Noeud_C * p = (T_Noeud_C*)malloc(sizeof(T_Noeud_C));
    p->cle = valeur;
    p->FD = NULL;
    p->FG = NULL;
    p->bdd = 1;
    p->bdg = 1;
    return p;
}


void cousu_prefixe(T_Arbre_C arbre){

    if(arbre!=NULL){
        printf("Noeud --> %d\n",arbre->cle);
        if(arbre->FG==NULL){
            printf("      --> FG(%d): NULL ",arbre->bdg);
        }else{
            printf("      --> FG(%d): %d ",arbre->bdg,arbre->FG->cle);
        }
        if(arbre->FD==NULL){
            printf("--> FD(%d): NULL \n",arbre->bdd);
        }else
            printf(" -- FD(%d): %d\n",arbre->bdd,arbre->FD->cle);
        if(arbre->bdg==0)
            cousu_prefixe(arbre->FG);
        if(arbre->bdd==0)
            cousu_prefixe(arbre->FD);
    }else{
        printf("ERREUR: arbre binaire cousu n'existe pas.\n");
        return;
    }
}

void cousu_inserer(int valeur,T_Arbre_C *arbre){
    T_Noeud_C * p = cousu_creer_noeud(valeur);
    T_Noeud_C * y = NULL;
    T_Noeud_C * x = *arbre;
    if(x==NULL){
        *arbre = p;
        return;
    }

    while(1){
        y = x;
        if(valeur < x->cle){
            if(x->bdg==0)
                x = x->FG;
            else
                break;
        } else if (valeur > x->cle){
            if(x->bdd==0)
                x = x->FD;
            else
                break;
        } else {
            break;
        }
    }
        if(valeur < y->cle){
            p->FD = y;
            p->FG = y->FG;
            y->FG = p;
            y->bdg = 0;
        }
        else if (valeur > y->cle) {
            p->FG = y;
            p->FD = y->FD;
            y->FD = p;
            y->bdd = 0;
        } else {
            printf("valeur deja dans l'arbre\n");
        }
}

void cousu_infixe(T_Arbre_C *arbre){
    T_Noeud_C * p = *arbre;
    if(p==NULL){
        printf("ERREUR: arbre binaire cousu n'existe pas.\n");
        return;
    }
    printf("---- parcours infixe d'un arbre binaire cousu----\n");
    //On commence le parcours par le nœud le plus à gauche de l’arbre
    while (p->FG!=NULL) {
        p = p->FG;
    }
    
    //Si le nœud courant n’est pas NULL alors l’afficher
    while(p!=NULL){
        printf("Noeud --> %d\n",p->cle);
        if(p->FG==NULL){
            printf("      --> FG(%d): NULL ",p->bdg);
        }else{
            printf("      --> FG(%d): %d ",p->bdg,p->FG->cle);
        }
        if(p->FD==NULL){
            printf("--> FD(%d): NULL \n",p->bdd);
        }else {
            printf(" -- FD(%d): %d\n",p->bdd,p->FD->cle);
        }
        //Si son fils droit est son successeur infixe
        if(p->bdd==1) {
            p = p->FD;
        //Si le nœud courant a un fils droit, alors trouver son successeur infixe (nœud le plus à gauche dans son sous-arbre droit)
        } else {
            p = p->FD;
            while(p->FG != NULL && p->bdg == 0){
                p = p->FG;
            }
        }
            
    }
}


void abr_to_cousu(T_Arbre abr, T_Arbre_C *clone, T_Noeud_C * parent){
    if(abr!=NULL){
        *clone = cousu_creer_noeud(abr->cle);
        abr_to_cousu(abr->FG, &((*clone)->FG), *clone);
        abr_to_cousu(abr->FD, &((*clone)->FD), *clone);
        T_Noeud_C * p = *clone;
        if (abr->FD!=NULL)
            (*clone)->bdd = 0;
        if (abr->FG!=NULL)
            (*clone)->bdg = 0;
        if(parent!=NULL){
            if(parent->FG!=NULL && p->cle== parent->FG->cle){
                while (p->bdd!=1)
                    p = p->FD;
                p->FD = parent;
            }
            if(parent->FD!=NULL && p->cle== parent->FD->cle){
                while (p->bdg!=1)
                    p = p->FG;
                p->FG = parent;
            }
        }
    }
    return;
}


void detruire_arbre_cousu(T_Arbre_C *abr){
    if (*abr == NULL)
        return;
    if((*abr)->bdg==0)
        detruire_arbre_cousu(&((*abr)->FG));
    if((*abr)->bdd==0)
        detruire_arbre_cousu(&((*abr)->FG));
    free(*abr);
    *abr = NULL;
    return;
}




void detruire_arbre_cousu(T_Arbre_C *arbre) {
    
    if (*arbre == NULL) {
        return;
    }

    if ((*arbre)->bdg == 0)
        detruire_arbre_cousu(&((*arbre)->FG));
    if ((*arbre)->bdd == 0)
        detruire_arbre_cousu(&((*arbre)->FD));


    *arbre = NULL;
    free(*arbre);

    return;
}
