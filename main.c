#include "tp4_abr.h"
#include "tp4_cousu.h"
#include <stdio.h>
#include <string.h>
int main(){
    
    //Création d'un arbre
    printf("Création d'un arbre\n");
    T_Arbre arbre = NULL;
    abr_inserer(11, &arbre);
    abr_inserer(11, &arbre);
    abr_inserer(2, &arbre);
    abr_inserer(13, &arbre);
    abr_inserer(1, &arbre);
    abr_inserer(6, &arbre);
    abr_inserer(17, &arbre);
    abr_inserer(15, &arbre);
    printf("-----Afficher l’ABR en préfixe-----\n");
    abr_prefixe(arbre);

    //suppression du noeud 11 et 1
    printf("\nSuppression du noeud 1 et 11\n");
    abr_supprimer(1,&arbre);
    abr_supprimer(11,&arbre);
    abr_prefixe(arbre);

    //clonage d'un arbre
    printf("\nClonage d'un arbre\n");
    T_Arbre clone = NULL;
    abr_clone(arbre, &clone, NULL);
    printf("-----Afficher l’ABR en préfixe-----\n");
    abr_prefixe(clone);
    //desctruction du clone
    printf("\nDestruction du clone\n");
    detruire_arbre(&clone);
    printf("-----Afficher l’ABR en préfixe-----\n");
    abr_prefixe(clone);
    printf("\n");
    printf("---- arbre cousu ---- \n");
    //création d'un arbre cousu
    printf("\nCréation d'un arbre cousu\n");
    T_Arbre_C arbre_cousu = NULL;
    cousu_inserer(11, &arbre_cousu);
    cousu_inserer(2, &arbre_cousu);
    cousu_inserer(13, &arbre_cousu);
    cousu_inserer(1, &arbre_cousu);
    cousu_inserer(6, &arbre_cousu);
    cousu_inserer(17, &arbre_cousu);
    cousu_inserer(15, &arbre_cousu);
    printf("-----Afficher l’ABR en préfixe-----\n");
    cousu_prefixe(arbre_cousu);
    cousu_infixe(&arbre_cousu);
    printf("\n");
    //création d'un arbre cousu à partir d'un ABR
    printf("\nCréation d'un arbre cousu à partir d'un ABR\n");
    T_Arbre_C clone_arbre_cousu = NULL;
    abr_to_cousu(arbre, &clone_arbre_cousu, NULL);
    printf("-----Afficher l’ABR en préfixe-----\n");
    cousu_prefixe(clone_arbre_cousu);

    printf("\nDétruire d'un arbre cousu à partir d'un ABR\n");
    detruire_arbre_cousu(&clone_arbre_cousu);
    printf("-----Afficher l’ABR en préfixe-----\n");
    cousu_prefixe(clone_arbre_cousu);
    int cle;
    T_Arbre ABR = NULL,cloneABR = NULL;
    T_Arbre_C cousuABR = NULL;
    int c = 3;
    
    while (c!= 11) {
        printf("\n");
        printf("1. Créer un ABR\n");
        printf("2. Afficher l’ABR en préfixe\n");
        printf("3. Insérer une valeur dans l’ABR\n");
        printf("4. Supprimer une valeur de l’ABR\n");
        printf("5. Cloner l’ABR\n");
        printf("6. Afficher le clone en préfixe\n");
        printf("7. Créer un arbre binaire cousu à partir d’un ABR\n");
        printf("8. Afficher l’arbre binaire cousu en préfixe\n");
        printf("9. Afficher l’arbre binaire cousu en infixe\n");
        printf("10. Insérer une valeur dans l’arbre binaire cousu\n");
        printf("11. Quitter\n\n");
        scanf("%d",&c);
        //c = getchar();
        //while(getchar() != '\n');
        switch (c) {

            case 1:
                ABR = NULL;
                printf("\nArbre créé \n");
                break;

            case 2:
                printf("\n-----Afficher l’ABR en préfixe-----\n");
                abr_prefixe(ABR);
                break;
            case 3:
                printf("\nEntrer une valeur à inserer: ");
                scanf("%d",&cle);
                abr_inserer(cle, &ABR);
                break;
            case '4':
                printf("\nEntrer une valeur à supprimer: ");
                scanf("%d",&cle);
                abr_supprimer(cle, &ABR);
                break;
            case '5':
                clone = NULL;
                abr_clone(ABR, &cloneABR, NULL);
                break;
            case 6:
                printf("\nAfficher le clone en préfixe\n");
                abr_prefixe(cloneABR);
                break;
            case 7:
                abr_to_cousu(ABR, &cousuABR, NULL);
                break;
            case 8:
                printf("\nAfficher l’arbre binaire cousu en préfixe\n");
                cousu_prefixe(cousuABR);
                break;
            case 9:
                printf("\nAfficher l’arbre binaire cousu en infixe\n");
                cousu_infixe(&cousuABR);
                break;
            case 10:
                printf("\nInsérer une valeur dans l’arbre binaire cousu\n");
                scanf("%d",&cle);
                cousu_inserer(cle, &cousuABR);
                break;
            default:
                break;

        }
    }
    return 0;
}
