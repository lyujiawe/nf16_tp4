#include "tp4_abr.h"

//Création d’un nœud
T_Noeud *abr_creer_noeud(int valeur){
    T_Noeud * p = (T_Noeud*)malloc(sizeof(T_Noeud));
    p->cle = valeur;
    p->FD = NULL;
    p->FG = NULL;
    return p;
}

//Affichage préfixe d’un arbre binaire de recherche
void abr_prefixe(T_Arbre abr){
    if (abr!=NULL) {
        printf("Noeud --> %d\n",abr->cle);
        if(abr->FG!=NULL)
            printf("      --> FG: %d ",abr->FG->cle);
        else
            printf("      --> FG: NULL ");
        if(abr->FD!=NULL)
            printf(" -- FD: %d\n",abr->FD->cle);
        else
            printf(" -- FD: NULL\n");
        abr_prefixe(abr->FG);
        abr_prefixe(abr->FD);
    } 
}

//Insertion d’une valeur dans un arbre binaire de recherche
void abr_inserer(int valeur,T_Arbre *abr){
    T_Noeud * p = abr_creer_noeud(valeur);
    T_Noeud * y = NULL;
    T_Noeud * x = *abr;
    
    while(x!=NULL){
        y = x;
        if(valeur < x->cle)
            x = x->FG;
        else if (valeur > x->cle)
            x = x->FD;
        else
            break;
    }

    if(y==NULL){

        *abr = p;

    } else if (y->cle == valeur) {
        
        printf("ERREUR: valeur deja dans l'arbre\n");
        return;

    } else {

        if(valeur < y->cle)
            y->FG = p;
        else
            y->FD = p;

    }
    printf("réussir à inserer\n");
    return;
}

//Suppression d’une valeur dans un arbre binaire de recherche
void abr_supprimer(int valeur,T_Arbre *abr){
    T_Noeud * q;
    T_Noeud * p = abr_rechercher(*abr, valeur);
    if (!p){
        printf("ERREUR: la valeur n'existe pas!\n");
        return;
    }
        
    T_Noeud * pere = noeudPere(p, abr);

    if (pere == NULL){

        if(p->FG==NULL && p->FD==NULL)
            *abr=NULL;
        else if(p->FG!=NULL && p->FD==NULL)
            *abr = p->FG;
        else if(p->FG==NULL && p->FD!=NULL)
            *abr = p->FD;
        else{

            T_Noeud * succ = p->FD;
            while (succ->FG != NULL) {
                succ = succ->FG;
            }
            p->cle = succ->cle;
            abr_supprimer(succ->cle, &(p->FD));
        }

    } else if(p->FG==NULL && p->FD==NULL){
        q = pere;
        if (p == q->FD) {
            q->FD = NULL;
        } else {
            q->FG = NULL;
        }
    }else if(p->FG!=NULL && p->FD==NULL){
        q = pere;
        if(q->FD==p)
            q->FD = p->FG;
        else
            q->FG = p->FG;
    }else if(p->FG==NULL && p->FD!=NULL){
        q = pere;
        if(q->FD==p)
            q->FD = p->FD;
        else
            q->FG = p->FD;
    }else{

        T_Noeud * succ = p->FD;
        while(succ->FG != NULL) {
            succ = succ->FG;
        }
        p->cle = succ->cle;
        abr_supprimer(succ->cle, &(p->FD));
    }
}

void abr_clone(T_Arbre original, T_Arbre *clone, T_Noeud* parent){
    if (original == NULL)
            return;

    *clone = abr_creer_noeud(original->cle);
    
    if (parent != NULL)
        if (parent->cle > (*clone)->cle)
            parent->FG = *clone;
        else
            parent->FD = *clone;

    if (original->FG != NULL) {

        abr_clone(original->FG, &((*clone)->FG), *clone);

    }

    if (original->FD != NULL) {

        abr_clone(original->FD, &((*clone)->FD), *clone);
    }
}

void detruire_arbre(T_Arbre *abr){
    if (*abr == NULL)
        return;

    detruire_arbre(&((*abr)->FG));
    detruire_arbre(&((*abr)->FD));
    
    *abr = NULL;
    free(*abr);
    return;
}


T_Noeud * noeudPere(T_Noeud *n,T_Arbre *abr){
    T_Noeud * p = *abr;
    if (n == p) {
        p = NULL;
        return p;
    }
    while(p->FD!=n && p->FG!=n){
        if(p->cle > n->cle)
            p = p->FG;
        else
            p = p->FD;
    }
    return p;
}

T_Noeud * abr_rechercher(T_Arbre abr,int k){
    T_Noeud * p = abr;
    while (p!=NULL && k!=p->cle) {
        if(k < p->cle)
            p = p->FG;
        else
            p = p->FD;
    }
    return p;
}
