#ifndef tp4_abr_h
#define tp4_abr_h

#include <stdio.h>
#include <stdlib.h>

typedef struct Noeud{
    int cle;
    struct Noeud * FG;
    struct Noeud * FD;
}T_Noeud;

typedef T_Noeud * T_Arbre;

//Création d’un nœud
T_Noeud *abr_creer_noeud(int valeur);

//Affichage préfixe d’un arbre binaire de recherche
void abr_prefixe(T_Arbre abr);



//Insertion d’une valeur dans un arbre binaire de recherche
void abr_inserer(int valeur,T_Arbre *abr);

//Suppression d’une valeur dans un arbre binaire de recherche
void abr_supprimer(int valeur,T_Arbre *abr);

void abr_clone(T_Arbre original, T_Arbre *clone, T_Noeud* parent);

void detruire_arbre(T_Arbre *abr);


//fonction en plus
T_Noeud * noeudPere(T_Noeud *n,T_Arbre *abr);

T_Noeud * abr_rechercher(T_Arbre abr,int k);

#endif /* tp4_abr_h */
