#ifndef tp4_cousu_h
#define tp4_cousu_h

#include <stdlib.h>
#include <stdio.h>
#include "tp4_abr.h"

typedef struct Noeud_C{
    int cle;
    struct Noeud_C * FG;
    struct Noeud_C * FD;
    int bdd;
    int bdg;
}T_Noeud_C;


typedef T_Noeud_C * T_Arbre_C;

T_Noeud_C * noeud_pere(T_Noeud_C *n,T_Arbre_C *abr);


T_Noeud_C *cousu_creer_noeud(int valeur);

void cousu_prefixe(T_Arbre_C arbre);

void cousu_inserer(int valeur,T_Arbre_C *arbre);

void cousu_infixe(T_Arbre_C *arbre);

void abr_to_cousu(T_Arbre abr, T_Arbre_C *clone, T_Noeud_C * parent);

void detruire_arbre_cousu(T_Arbre_C *abr);
//Fonctions en plus



#endif /* tp4_cousu_h */
